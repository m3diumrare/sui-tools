# SUI TOOLS

In this repository you can find tools and guides for Sui network validator servers.

## Guides

- [SUI fullnode as validator backup](https://gitlab.com/blockscope-net/sui-tools/-/blob/main/sui-fullnode-as-validator-backup.md): guide to set up a fullnode and use it as a validator backup server. (WIP)

## Tools

- [Sui Sonar](https://discord.gg/QUhNjhdaUa): monitoring platform for validator nodes, integrates with: Discord, Telegram, Slack, and Pager Duty. [Sonar Docs](https://gitlab.com/blockscope-net/sonar-docs/-/wikis/home)
- [Grafana Dashboard (Narwhal)](https://gitlab.com/blockscope-net/sui-tools/-/blob/main/sui-validator-grafana-dashboard.json): Grafana dashboard for individual nodes with most important metrics taken from official Sui Grafana dashboards.
- [Grafana Dashboard v2 (Mysticeti)](https://gitlab.com/blockscope-net/sui-tools/-/blob/main/sui-validator-grafana-dashboard.json): Grafana dashboard for individual nodes with most important metrics taken from official Sui Grafana dashboards.

### Grafana Dashboard

This dashboard aims to provide a series of useful charts and data to monitor the health of your testnet and mainnet validator nodes. The dashboard has a select for selecting different server instances, so if you include several servers in your Prometheus config and the corresponding variables in the Grafana dashboard you will have access to inspecting all your instances in the same dashboard.

![Dashboard screenshot 1](image-1.png)

![Dashboard screenshot 2](image-2.png)

_Prometheus Config_

```yaml
- job_name: "sui-metrics"
  static_configs:
    - targets: ["1.1.1.1:9184"]
      labels:
        alias: sui-testnet-metrics
        instance: "Sui Testnet"
    - targets: ["2.2.2.2:9184"]
      labels:
        alias: sui-mainnet-metrics
        instance: "Sui Mainnet"
```

_Grafana Config_

You need the following variables to be able to access the instances you declare in your prometheus through the Host selector on top of the Grafana dashboard.

![Config vars](image-4.png)

Once declared you should see your instances in the Host selector.

NOTE: For Mysticeti consensus algorithm you need to declare the host variable with the name of your validator.

![Dashboard selectors](image-3.png)

## Authors and acknowledgment

Tools and resources developed by the Blockscope team for the Sui project.

## License

Copyright 2023 Blockscope.net

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Project status

WIP: our team will be uploading new tools and improvements so we keep help growing the Sui ecosystem.
